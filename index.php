<?php
    require_once('config.php');
    // if(!$_SESSION['logado'])
    // {
    //     header('location:index.php');
    // }
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dynamic Project</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div id="estrutura">
        <div id="topo"></div>
        <div><p>(<a href="op_login.php?sair=true">sair</a>)<?php echo isset($_SESSION['nome_user'])?$_SESSION['nome_user']:''; ?></p>
        </div>
        <div id="menu">
            
            <ul>
                <li><a href="index.php?link=1&valor=12">Home</a></li>
                <li><a href="index.php?link=1">Serviços</a></li>
                <li><a href="index.php?link=1">Produtos</a></li>
                <li><a href="index.php?link=1">Contatos</a></li>
                <li><a href="index.php?link=20">Posts</a></li>
                <li><a href="frm_usuario.php">Cadastrar</a></li>
                <li><a href="frm_login.php">Login</a></li>
            </ul>
        </div>
        <div id="banner" class="banner"></div>
        <div id="corpo">
            <div id="esquerda" class="esquerda">
                <h1>Produtos</h1>
                <li><a href="index.php?link=5">Produto 1</a></li>
                <li><a href="index.php?link=">Produto 2</a></li>
                <li><a href="index.php?link=">Produto 3</a></li>
                <li><a href="index.php?link=">Produto 4</a></li>
            </div>
            <div id="centro">
                <?php //Aqui começamos a falar de php
                    $link = isset($_GET['link'])?$_GET['link']:'';                   
                    $pag[1] = 'home.php';
                    $pag[5] = 'produto.php';
                    $_SESSION['id_not'] = filter_input(INPUT_GET,'idnoticia');
                    $_SESSION['id_post'] = filter_input(INPUT_GET,'idpost'); 
                    $pag[10] = 'conteudo_post.php';                    
                    $pag[3] = 'conteudo_noticia.php';
                    $pag[7] = 'frm_usuario.php';
                    if(!empty($link))
                    {
                        if(file_exists($pag[$link]))
                        {
                            include($pag[$link]);
                        }
                        else
                        {
                            include($pag[1]);
                        }
                    }
                    else
                    {
                        include($pag[1]);
                    }
                ?>
            </div>
            <div id="direita">
                <h3>Noticias</h3><br>
                <div id="noticias">
                    <?php
                        $nots = Noticia::GetListNoticia();
                        foreach($nots as $not)
                        {
                            if($not['noticia_ativo']==1)
                            {
                    ?>
                    <h3><img src="admin/foto/<?php echo $not['img_noticia'];?>"alt="" width="80" height="80"></h3>
                    <div id="itens-noticias">
                        <span><?php echo $not['data_noticia'];?></span>
                        <a href="index.php?link=3&idnoticia=<?php echo $not['id_noticia'];?>">
                        <?php echo $not['titulo_noticia'];?></a>
                    </div>
                    <?php }}?>
                </div>
                <br><br>
                <hr>
                <h3>Post</h3><br>
                <div id="post">
                    <?php
                        require_once('config.php');
                        $posts = Post::GetListPost();
                        foreach($posts as $post)
                        {
                            if($post['post_ativo']==1)
                            {
                    ?>
                    <div id="itens-post">
                    <span><?php echo $post['data_post'];?></span>
                    <a href="index.php?link=10&idpost=<?php echo $post['id_post'];?>">
                    <?php echo $post['titulo_post'];?></a>
                    </div>
                    <?php }}?>
                </div>
            </div>
            <div>
                Area do adenor
                <a href="admin/index.php">Acesso a area do adenor</a>
            </div>
            <div id="rodape">
                &copy; - Todos os direitos reservados
            </div>
        </div>
    </div>
</body>
</html>