<?php
/*
    include_once('config.php');
    if(isset($_SESSION['logado']))
    {
        if($_SESSION['logado'])
        {
            header('location:produto.php');
        }
    }*/
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Area do usuario</title>
    <link rel="stylesheet" href="admin/css/style.css">
</head>
<body>
    <div id="box-login">
        <div id="formulario-login">
            <form id="frmlogin" name="frmlogin" action="op_login.php" method="post">
                <fieldset>
                    <legend>Faça login para obter mais recursos do site</legend>
                    <label for=""><span>Login</span></label>
                    <input type="text" name="txt_login" id="txt_login">

                    <label for=""><span>Senha</span></label>
                    <input type="password" name="txt_senha" id="txt_senha">

                    <input type="submit" name="login" id="login" value="logar" class="botao">
                    <input type="submit" name="cadastro" id="cadastro" value="Cadastrar-se" class="botao">

                    <br>
                    <span><?php echo (isset($_GET['msg']))?$_GET['msg']:"";  ?></span>
                </fieldset>
            </form>
        </div>
    </div>
</body>
</html>