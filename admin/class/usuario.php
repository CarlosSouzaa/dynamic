<?php
    class Usuario
    {
        //* Atributos
        private $id;
        private $nome;
        private $login;
        private $senha;

        //Métodos de acesso
        // Id
        public function getId()
        {
            return $this->id;
        }
        public function setId($value)
        {
            $this->id = $value;
        }
        // Nome
        public function getNome()
        {
            return $this->nome;
        }
        public function setNome($value)
        {
            $this->nome = $value;
        }
        //* Login
        public function getLogin()
        {
            return $this->login;
        }
        public function setLogin($value)
        {
            $this->login = $value;
        }
        //* Senha
        public function getSenha()
        {
            return $this->senha;
        }
        public function setSenha($value)
        {
            $this->senha = $value;
        }

        //* Métodos da classe
       
        public function Inserir()
        {
            $sql = new Sql();
            $results = $sql->select('CALL sp_insert_user(:nome,:login,:senha)',
            array
            (
                ':nome'=>$this->getNome(),
                ':login'=>$this->getLogin(),
                ':senha'=>md5($this->getSenha())
            ));
            if(count($results)>0)
            {
                $this->setData($results[0]);
            }
        }
        //* Validar login
        public function efetuarlogin($_login, $_senha)
        {
            $sql = new SQL();
            $senhaMd5 = md5($_senha);
            $results = $sql->select('SELECT * FROM usuario WHERE login = :login AND senha = :senha', 
            array
            (
                ':login'=>$_login,
                ':senha'=>$senhaMd5
            ));
            if(count($results)>0)
            {
                $this->setData($results[0]);
            }
        }
        // Passa dados aos atributos
        public function setData($data)
        {
            $this->setId($data['id']);
            $this->setNome($data['nome']);
            $this->setLogin($data['login']);
            $this->setSenha($data['senha']);
        }

        //Construtor
        public function __construct($_iduser="",$_nome="",$_login="",$_senha="")
        {
            $this->id = $_iduser;
            $this->nome = $_nome;
            $this->login= $_login;
            $this->senha = $_senha;   
        }
    }

?>