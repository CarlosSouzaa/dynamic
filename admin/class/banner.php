<?php
    Class Banner
    {
        //* -------------------------------------Atributos da classe----------------------------------
        public $id_banner;
        public $titulo_banner;
        public $link_banner;
        public $img_banner;
        public $alt;

        //* -------------------------------------Métodos de acesso-----------------------------------
        //*  ID Banner
        public function getId()
        {
            return $this->id_banner;
        }
        public function setId($value)
        {
            $this->id_banner = $value;
        }
        //* Titulo banner
        public function getTitulo()
        {
            return $this->titulo_banner;
        }
        public function setTitulo($value)
        {
            $this->titulo_banner = $value;
        }
        //* Link Banner
        public function getLink() 
        { 
            return $this->link_banner;
        }
        public function setLink($value)
        {
            $this->link_banner = $value;
        }
        //* Imagem do banner
        public function getImg()
        {
            return $this->img_banner;
        }
        public function setImg($value)
        {
            $this->img_banner = $value;
        }
        //* Alt Do banner
        public function getAlt()
        {
            return $this->alt;
        }
        public function setAlt($value)
        {
            $this->alt = $value;
        }

        //* -----------------------------------------Métodos-----------------------------------------------
        

        //* Inserir
        public function Inserir()
        {
            $sql = new Sql();
            $sql->select('CALL sp_banner_insert(:ban,:titulo,:link,:img,:alt)', 
            array
            (
                ':ban'=$this->getId(),
                ':titulo'=$this->getTitulo(),
                ':link'=$this->getLink(),
                ':img'=$this->getImg(),
                ':alt'=$this->getAlt()
            ));
        }
        //* Alterar
        public function Alterar($_id,$_titulo, $_link, $_img,$_alt)
        {
            $sql = new Sql();
            $sql->query('UPDATE banner SET titulo_banner = :titulo, link_banner = :link, img_banner = :img, alt = :alt WHERE id = :id',
            array
            (
                ':id'=$_id,
                ':titulo'=$_titulo,
                ':link'=$_link,
                ':img'=$_img,
                ':alt'=$_alt
            )); 
        }
        // * Deletar
        public function Delete($_id)
        {
            $sql = new Sql();
            $sql->query('DELETE * FROM banner WHERE id = :id',
            array
            (
                ':id' = $_id
            ));
        }

        //* Gerar Lista

        public static function GetListBanner()
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM banner order by id_banner');
        }
        
        public function __construct($_id='',$_titulo='',$_link,$_img='',$_alt='')
        {
            $this->id_banner = $_id;
            $this->titulo_banner = $_titulo;
            $this->img_banner = $_img;
            $this->alt = $_alt;
            $this->link_banner = $_link;
        }    


    }


?>