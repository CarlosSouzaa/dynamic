<?php
    class Categoria
    {
        //todo Atributos da classe
        private $id_categoria;
        private $categoria;
        private $cat_ativo;

        //todo Métodos de acesso
        //* Id categoria
        public function getId()
        {
            return $this->id_categoria;
        }
        public function setId($value)
        {
            $this->id_categoria = $value;
        }
        //* Categoria (nome da categoria)
        public function getCategoria()
        {
            return $this->categoria;
        }
        public function setCategoria($value)
        {
            $this->categoria = $value;
        }
        //* Categoria Ativa
        public function getAtivo()
        {
            return $this->cat_ativo;
        }
        public function setAtivo($value)
        {
            $this->cat_ativo = $value;
        }
        //todo Funções
        //* Buscar categoria por id
        public function buscaPorId($_id)
        {
            $sql = new Sql();
            $resultado = $sql->select("SELECT * FROM categoria WHERE id = :id",
            array
            (
                'id'=>$_id
            ));
            if(count($resultado)>0)
            {
                $this->setData($resultado[0]);
            }
        }
        //* Lista de categorias
        public static function getListCat()
        {
            $sql =  new Sql();
            return $sql->select("SELECT * FROM categoria order by categoria");
        }
        //* Passar dados aos atributos
        public function setData($data)
        {
            $this->setId($data['id_categoria']);//? Aqui deve se passar id ou id categoria como parametro? RESPOSTA: IGUAL ESTA NO BANCO!
            $this->setCategoria($data['categoria']);//? Mesma coisa
            $this->setAtivo($data['cat_ativo']);//? Mesma coisa
        }
        //* Método Inserir Categoria
        public function insert()
        {
            $sql = new Sql();
            $resultado = $sql->select('CALL sp_categoria_insert(:categoria,:ativo)',
            array
            (
                ':categoria'=>$this->getCategoria(),
                ':ativo'=>$this->getAtivo()
            ));
            if(count($resultado)>0)
            {
                $this->setData($resultado[0]);
            }
        }
        //* Método para atualizar dados da lista
        public function update($_id, $_cat, $_ati)
        {
            $sql = new Sql();
            $sql->query("UPDATE categoria SET categoria = :categoria, cat_ativo = :ativo WHERE id_categoria = :id",
            array
            (
                ':id_'=>$_id,
                ':categoria'=>$_cat,
                ':ativo'=>$_ati
            ));
        }
        //* Método para deletar uma categoria
        public function deletar()
        {
            $sql = new Sql();
            $sql->query('DELETE FROM categoria WHERE id = :id',
            array
            (
                ':id'=>$this->getId()
            ));
        }
        //* Método Construtor
        public function __construct($_categoria="", $_ativo="")
        {
            $this->categoria = $_categoria;
            $this->cat_ativo = $_ativo;
        }
    }
?>