<?php
    class Administrador
    {
        //* Atributos da classe 
        private $id;
        private $nome;
        private $email;
        private $login;
        private $senha;

        //* Métodos de acesso {Get e Set}
        //* Id
        public function getId()
        {
            return $this->id;
        }
        public function setId($value)
        {
            $this->id = $value ;
        }
        //* Nome
        public function getNome()
        {
            return $this->nome;
        }
        public function setNome($value)
        {
            $this->nome = $value ;
        }
        //* Email
        public function getEmail()
        {
            return $this->email;
        }
        public function setEmail($value)
        {
            $this->email = $value ;
        }
        //* Login
        public function getLogin()
        {
            return $this->login;
        }
        public function setLogin($value)
        {
            $this->login = $value ;
        }
        //* Senha
        public function getSenha()
        {
            return $this->senha;
        }
        public function setSenha($value)
        {
            $this->senha = $value ;
        }
        //* Busca por Id
        public function loadById($_id)
        {
            $sql = new Sql(); //! $sql = Instância de Sql
            $results = $sql->select("SELECT * FROM administrador WHERE id = :id", array(':id' => $_id));
            if (count($results)>0)
            {
                $this->setData($results[0]);
            }
        }
        //* Gerar Lista adm
        //* '::' chama um metodo estático da classe 
        public static function getList()
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM administrador');
            //* Realiza um busca no banco trazendo tudo que é administrador onde o criterio é nome.
        }
        //* Pesquisa por parte do nome
        //todo: 'LIKE' junto ao '%'ed'%' retorna tudo que tenha 'ed' no nome inteiro como: 'Edson' e 'Eduardo' entre outros.
        public static function search($nome_adm)
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM administrador WHERE nome LIKE :nome', array(':nome'=>"%".$nome_adm."%"));
        }
        //* Concluir o login 
        public function efetuarlogin($_login, $_senha)
        {
            $sql = new SQL();
            $senhaMd5 = md5($_senha);
            $results = $sql->select('SELECT * FROM administrador WHERE login = :login AND senha = :senha', 
            array
            (
                ':login'=>$_login,
                ':senha'=>$senhaMd5
            ));
            if(count($results)>0)
            {
                $this->setData($results[0]);
            }
        }
        //* Passa dados aos Atributos
        public function setData($data)
        {   
            $this->setId($data['id']);
            $this->setNome($data['nome']);
            $this->setEmail($data['email']);
            $this->setLogin($data['login']);
            $this->setSenha($data['senha']);
        }
        //* Método Inserir Administrador
        public function insert()
        {
            $sql = new Sql();
            $results = $sql->select("CALL sp_adm_insert(:nome, :email, :login, :senha)",
            array
            (
                ':nome'=>$this->getNome(),
                ':email'=>$this->getEmail(),
                ':login'=>$this->getLogin(),
                ':senha'=>md5($this->getSenha())
            ));
            if(count($results)>0)
            {
                $this->setData($results[0]);
            }
        }
        //* Método Atualizar Dados do Administrador
        public function update($_id, $_nome, $_email, $_login)
        {
            $sql = new Sql();
            $sql->query("UPDATE administrador SET nome = :nome, email = :email, login = :login WHERE id = :id",
            array
            (
                ':id'=>$_id,
                ':nome'=>$_nome,
                ':email'=>$_email,
                ':login'=>$_login
            ));
        }
        //* Método deletar um administrador
        public function deletar()
        {
            $sql = new Sql();
            $sql->query('DELETE FROM administrador WHERE id = :id',
            array
            (
                ':id'=>$this->getId()
            ));
            
        }
        //* Método Construtor
        public function __construct($_nome="", $_email="", $_login="", $_senha="")
        {
            $this->nome = $_nome;
            $this->email = $_email;
            $this->login = $_login;
            $this->senha = $_senha;
        }

    }
?>