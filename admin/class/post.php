<?php
    class Post
    {    
        //* Atributos da classe

        public $id_post;
        public $id_categoria;
        public $titulo_post;
        public $descricao_post;
        public $img_post;
        public $visita;
        public $data_post;
        public $post_ativo;

        //* Metodos de acesso
        //* Modelo
        // public function get()
        // {
        //     return $this->;
        // }
        // public function set($value)
        // {
        //     $this-> = $value;
        // }

        //* Id post
        public function getId()
        {
            return $this->id_post;
        }
        public function setId($value)
        {
            $this->id_post = $value;
        }
        //* Id Categoria
        public function getIdCat()
        {
            return $this->id_categoria;
        }
        public function setIdCat($value)
        {
            $this->id_categoria = $value;
        }
        //* Titulo do post
        public function getTitulo()
        {
            return $this->titulo_post;
        }
        public function setTitulo($value)
        {
            $this->titulo_post = $value;
        }
        //* Descrição do post
        public function getDesPost()
        {
            return $this->descricao_post;
        }
        public function setDescPost($value)
        {
            $this->descricao_post = $value;
        }
        //* Imagem do post
        public function getImgPost()
        {
            return $this->img_post;
        }
        public function setImgPost($value)
        {
            $this->img_post = $value;
        }
        //* Visitas no post
        public function getVisita()
        {
            return $this->visita;
        }
        public function setVisita($value)
        {
            $this->visita = $value;
        }
        //* Data do post
        public function getDataPost()
        {
            return $this->data_post;
        }
        public function setDataPost($value)
        {
            $this->data_post = $value;
        }
        //* Post Ativo
        public function getPostAtivo()
        {
            return $this->post_ativo;
        }
        public function setPostAtivo($value)
        {
            $this->post_ativo = $value;
        }

        //* Métodos
        //*Buscar post por id
        public function BuscarPorId($_id)
        {
            $sql = new Sql();
            $resultado = $sql->select('SELECT * FROM post WHERE id_post = :id',
            array
            (
                ':id'=>$_id
            ));
            if(count($resultado)>0)
            {
                $this->setData($resultado[0]);
            }
        }
        //* Lista de Posts
        public static function GetListPost()
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM post');
        }
        //* Passando dados aos atributos
        public function setData($data)
        {
            $this->setId($data['id_post']);
            $this->setIdCat($data['id_categoria']);
            $this->setTitulo($data['titulo_post']);
            $this->setDescPost($data['descricao_post']);
            $this->setImgPost($data['img_post']);
            $this->setVisita($data['visitas']);
            $this->setDataPost($data['data_post']);
            $this->setPostAtivo($data['post_ativo']);
        }
        //* Metodo Inserir post
        public function Insert()
        {
            $sql = new Sql();
            $resultado = $sql->select('CALL sp_post_insert(:id_cat, :titulo, :desc, :img, 0, :data, :ativo)',
            array
            (
                ':id_cat'=>$this->getIdCat(),
                ':titulo'=>$this->getTitulo(),
                ':desc'=>$this->getDesPost(),
                'img'=>$this->getImgPost(),
                'data'=>$this->getDataPost(),
                'ativo'=>$this->getPostAtivo()
            ));
            if(count($resultado)>0)
            {
                $this->setData($resultado[0]);
            }
        }
        //* Método para atualizar dados da lista de post
        public function update($_titulo,$_desc,$_img,$_data,$_ativo)
        {
            $sql = new Sql();
            $sql->query('UPDATE post SET titulo_post = :titulo, descricao_post = :descricao, img_post = :img, data_post = :data_p, post_ativo = :ativo ',
            array
            (
                ':titulo'=>$_titulo,
                'descricao'=>$_desc,
                ':img'=>$_img,
                ':data_p'=>$_data,
                ':ativo'=>$_ativo
            ));
        }
        //* Deletar Post
        public function delete()
        {
            $sql = new Sql();
            $sql->query('DELETE FROM post WHERE id = :id',
            array
            (
                ':id'=>$this->getId()
            ));
        }

        //* Contabilizar posts visitados
        public function UpdatePost($_id)
        {
            $sql = new Sql();
            $sql->query('UPDATE post SET  visitas = visitas + 1 WHERE id_post = :id', 
            array
            (
                ':id'=>$_id
            ));
        }
        //* Método Construtor
        public function __construct($_idcat="",$_titulo="",$_desc="",$_img="",$_data="",$_ativo="")
        {
            $this->id_categoria = $_idcat;
            $this->titulo_post = $_titulo;
            $this->descricao_post = $_desc;
            $this->img_post = $_img;
            $this->data_post = $_data;
            $this->post_ativo = $_ativo;    
        }


    }

?>