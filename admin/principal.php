<?php
    // require_once('../config.php');
    // if(!$_SESSION['logado'])
    // {
    //     header('Location: index.php');
    // }
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Area Adminstrativa</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div id="principal">
        <div id="cabecalho">
            <!-- //*Criação do cabeçalho-->
            <div id="titulo_topo">
                <img src="../img/a-admin.png" alt="">
                <br>
                <p>(<a href="op_administrador.php?sair=true">sair</a>)<?php echo $_SESSION['nome_adm']?></p>
            </div>
        </div> <!-- //*Fim do cabeçalho-->
    </div>
    <div id="corpo">
        <div id="esquerdo"><!-- //*Criação da sessão esquerda -->
            <!-- //!Sessão - Alterar depois para a Web Semântica  -->
            <!--//*Administrador-->   
            <div id="sessao">Administrador 
                <ul>
                    <li><a href="principal.php?link=2">Cadastrar</a></li>
                    <li><a href="principal.php?link=3">Editar</a></li>
                </ul>
            </div>
            <!--//*Categoria-->          
            <div id="sessao">Categoria
                <ul>
                    <li><a href="principal.php?link=4">Cadastrar</a></li>
                    <li><a href="principal.php?link=5">Editar</a></li>
                </ul>
            </div>
            <!--//*Posts-->  
            <div id="sessao">Posts
                <ul>
                    <li><a href="principal.php?link=6">Cadastrar</a></li>
                    <li><a href="principal.php?link=7">Editar</a></li>
                </ul>
            </div>
            <!--//*Noticias-->  
            <div id="sessao">Noticia
                <ul>
                    <li><a href="principal.php?link=8">Cadastrar</a></li>
                    <li><a href="principal.php?link=9">Editar</a></li>
                </ul>
            </div>
            <!--//*Banner-->  
            <div id="sessao">Banner
                <ul>
                    <li><a href="principal.php?link=10">Cadastrar</a></li>
                    <li><a href="principal.php?link=11">Editar</a></li>
                </ul>
            </div>
        </div><!-- //*Fim do esquerdo-->
        <div id="direito"><!-- //*Criação da sessão direita -->         
            <?php
            //!Sessão - Alterar depois para a Web Semântica 
                if(isset($_GET['link']))// todo / isset = Se tiver algum valor atribuido.
                {
                    $link = $_GET['link'];
                    $pag[1]= "home.php";
                    $pag[2]= "frm_administrador.php";
                    $pag[3]= "lista_adm.php";
                    $pag[4]= "frm_categoria.php";
                    $pag[5]= "lista_categoria.php";
                    $pag[6]= "frm_post.php";
                    $pag[7]= "lista_post.php";
                    $pag[8]= "frm_noticia.php";
                    $pag[9]= "lista_noticia.php";
                    $pag[10]= "frm_banner.php";
                    $pag[11]= "lista_banner.php";
                }
                if(!empty($link))// todo / ! = se não;
                {
                    if(file_exists($pag[$link]))
                    {
                        include $pag[$link];
                    }
                    else
                    {
                        include $pag[1];
                    }
                }
                else
                {
                    include "home.php";
                }
            ?>
        </div> <!-- //*Fim do direito -->
    </div>
</body>
</html>