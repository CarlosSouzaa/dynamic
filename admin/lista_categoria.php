<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Lista categoria</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <table id="td_categoria" width="100%" border='0' cellpadding="0" cellspacing="1" bgcolor="660000">
        <tr bgcolor="#993300" align="center">
            <th whidth="15%" height="2" font size="2" color="#ffffff">Codigo</th>
            <th whidth="60%" height="2" font size="2" color="#ffffff">Categoria</th>
            <th whidth="15%" height="2" font size="2" color="#ffffff">Ativo</th>
            <th colspan="2"  font-size="2" color="#ffffff">Opções</th>
        </tr>
        <?php
            require_once('../config.php');
            $cats = Categoria::getListCat();
            foreach($cats as $cat)
            {
        ?>
        <tr>
            <td><font size="2" face="verdana, arial" color="#fff"><?php echo$cat['id_categoria']?></font></td>
            <td><font size="2" face="verdana, arial" color="#fff"><?php echo$cat['categoria']?></font></td>
            <td><font size="2" face="verdana, arial" color="#fff"><?php echo$cat['cat_ativo']=='1'?'Sim':'Não';?></font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#fff"><a href="<?php echo 'alterar_cat.php?id='.$cat['id_categoria'].'&cat='.$cat['categoria'].'&ativo='.$cat['cat_ativo']?>">Alterar</a></font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#fff"><a href="<?php echo "op_categoria.php?excluir=1&id_cat=".$cat['id_categoria']?>">Excluir</a></font></td>
        </tr>
        <?php }?>
    </table>
</body>
</html>