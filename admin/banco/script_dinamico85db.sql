-- create database dinamico85db;

-- use dinamico85db;

-- SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
-- SET AUTOCOMMIT = 0;
-- START TRANSACTION;
-- SET time_zone = "+00:00";

-- -- --------------------------------------------------------

-- --
-- -- Estrutura da tabela `administrador`
-- --

-- CREATE TABLE `administrador` (
--   `id` int(11) NOT NULL,
--   `nome` varchar(200) NOT NULL,
--   `email` varchar(200) NOT NULL,
--   `login` varchar(100) NOT NULL,
--   `senha` varchar(50) NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --
-- -- Extraindo dados da tabela `administrador`
-- --

-- INSERT INTO `administrador` (`id`, `nome`, `email`, `login`, `senha`) VALUES
-- (1, 'Wellington Vieira', 'wellington@dev.senac', 'wellington', '123456');

-- -- --------------------------------------------------------

-- --
-- -- Estrutura da tabela `banner`
-- --

-- CREATE TABLE `banner` (
--   `id_banner` int(11) NOT NULL,
--   `titulo_banner` varchar(255) NOT NULL,
--   `link_banner` varchar(255) NOT NULL,
--   `img_banner` varchar(150) NOT NULL,
--   `alt` varchar(255) NOT NULL,
--   `banner_ativo` varchar(1) NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -- --------------------------------------------------------

-- --
-- -- Estrutura da tabela `categoria`
-- --

-- CREATE TABLE `categoria` (
--   `id_categoria` int(11) NOT NULL,
--   `categoria` varchar(150) NOT NULL,
--   `cat_ativo` varchar(1) NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -- --------------------------------------------------------

-- --
-- -- Estrutura da tabela `noticias`
-- --

-- CREATE TABLE `noticias` (
--   `id_noticia` int(11) NOT NULL,
--   `id_categoria` int(11) NOT NULL,
--   `titulo_noticia` varchar(255) NOT NULL,
--   `img_noticia` varchar(100) NOT NULL,
--   `visita_noticia` int(11) NOT NULL,
--   `data_noticia` date NOT NULL,
--   `noticia_ativo` varchar(1) NOT NULL,
--   `noticia` text NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -- --------------------------------------------------------

-- --
-- -- Estrutura da tabela `post`
-- --

-- CREATE TABLE `post` (
--   `id_post` int(11) NOT NULL,
--   `id_categoria` int(11) NOT NULL,
--   `titulo_post` varchar(250) NOT NULL,
--   `descricao_post` text NOT NULL,
--   `img_post` varchar(200) NOT NULL,
--   `visitas` int(11) NOT NULL,
--   `data_post` date NOT NULL,
--   `post_ativo` varchar(1) NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --
-- -- Indexes for dumped tables
-- --

-- --
-- -- Indexes for table `administrador`
-- --
-- ALTER TABLE `administrador`
--   ADD PRIMARY KEY (`id`);

-- --
-- -- Indexes for table `banner`
-- --
-- ALTER TABLE `banner`
--   ADD PRIMARY KEY (`id_banner`);

-- --
-- -- Indexes for table `categoria`
-- --
-- ALTER TABLE `categoria`
--   ADD PRIMARY KEY (`id_categoria`);

-- --
-- -- Indexes for table `noticias`
-- --
-- ALTER TABLE `noticias`
--   ADD PRIMARY KEY (`id_noticia`);

-- --
-- -- Indexes for table `post`
-- --
-- ALTER TABLE `post`
--   ADD PRIMARY KEY (`id_post`);

-- --
-- -- AUTO_INCREMENT for dumped tables
-- --

-- --
-- -- AUTO_INCREMENT for table `administrador`
-- --
-- ALTER TABLE `administrador`
--   MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

-- --
-- -- AUTO_INCREMENT for table `banner`
-- --
-- ALTER TABLE `banner`
--   MODIFY `id_banner` int(11) NOT NULL AUTO_INCREMENT;

-- --
-- -- AUTO_INCREMENT for table `categoria`
-- --
-- ALTER TABLE `categoria`
--   MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT;

-- --
-- -- AUTO_INCREMENT for table `noticias`
-- --
-- ALTER TABLE `noticias`
--   MODIFY `id_noticia` int(11) NOT NULL AUTO_INCREMENT;

-- --
-- -- AUTO_INCREMENT for table `post`
-- --
-- ALTER TABLE `post`
--   MODIFY `id_post` int(11) NOT NULL AUTO_INCREMENT;
-- COMMIT;

-- delimiter //
-- create procedure sp_adm_insert (_nome varchar(255),_email varchar(200),_login varchar(100),_senha varchar(100))
-- begin
-- 	  insert into administrador (nome,email,login,senha) values (_nome,_email,_login,_senha);
--     select * from administrador where id = LAST_INSERT_ID();
-- end;

-- delimiter //
-- create procedure sp_banner_insert (_titulo varchar(255),_link varchar(255),_img varchar(255),alt varchar(255),_ativo varchar(1))
-- begin
-- 	  insert into banner (titulo_banner,link_banner,img_banner,alt,banner_ativo) values (_titulo, _link, _img, alt, _ativo);
--     select * from banner where id_banner = LAST_INSERT_ID();
-- end;

-- delimiter //
-- create procedure sp_categoria_insert (categoria varchar(255), ativo varchar(1))
-- begin
--   insert into categoria (categoria, cat_ativo) values (categoria, ativo);
--   select * from categoria where id_categoria = LAST_INSERT_ID();
-- end;

-- delimiter //
-- create procedure sp_noticias_insert (_id_categoria int(11),_titulo varchar(255),_img varchar(255), _visita int(11), _data_noticia date, _ativo varchar(1), _noticia text)
-- begin
--   insert into noticias (id_categoria,titulo_noticia,img_noticia,visita_noticia,data_noticia,noticia_ativo,noticia) values (_id_categoria,_titulo,_img,_visita,_data_noticia,_ativo,_noticia);
--   select * from noticias where id_noticia = LAST_INSERT_ID();
-- end;
-- delimiter //
-- create procedure sp_post_insert (id_categoria int(11),titulo varchar(255),descricao varchar(255),img varchar(255),visita int(11),_data_post date,_ativo varchar(1))
-- begin
--   insert into post (id_categoria,titulo_post,descricao_post,img_post,visitas,data_post,post_ativo) values (_id_categoria,_titulo,_descricao,_img,_visita,_data_post,_ativo);
--   select * from post where id_post = LAST_INSERT_ID();
-- end;
-- DELIMITER //
-- create procedure sp_insert_user (_nome varchar(100), _login varchar (100), _senha varchar(100))
-- BEGIN
-- 	insert into usuario (nome, login,senha) values (_nome,_login,_senha);
--     select * from usuario where id_user = last_insert_id();
-- END //