<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Formulario administrador</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div id="boxcadastro">
    <div id="formulario-menor">
        <form id="frm_administrador" action="op_administrador.php" name="frm_administrador" method="POST">
            <fieldset>
                <legend>Cadastrar Adminitrador</legend>
                    <!--Id Do adminstrador-->
                    <input type="hidden" id="id" name="id">
                    <!--Nome administrador-->
                    <label>Nome Completo *obrigatorio<br>
                        <input type="text" name="txt_nome" id="txt_nome" value=""required>
                    </label> 
                    <!--Login do administrador--> 
                    <label>                
                        Login *obrigatorio<br>
                        <input type="text" name="txt_login" id="txt_login" value="" required>
                    </label>
                    <!--Email do adm-->
                    <label>Email *obrigatorio<br>
                        <input type="email" name="txt_email" id="txt_email" value="" required>
                    </label>
                    <!--Senha do Adm-->
                    <label>Senha *obrigatorio<br>
                        <input type="password" name="txt_senha" id="txt_senha" value="" required> 
                    </label>
                    <!--Botão para efetuar o cadastro do adm-->
                    <label>
                        <input type="submit" name="btn_cadastrar" value="Cadastrar" class="botao">
                       </label>              
            </fieldset>
        </form>
    </div>
</div>
</body>
</html>
