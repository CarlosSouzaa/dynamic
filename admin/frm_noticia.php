<div id="box-cadastro">
<div id="formulario-menor">
    <form action="op_noticia.php" method="post" enctype="multipart/form-data">
        <fieldset>               
            <label>
                <select name="id_categoria" id="id_categoria">            
                    <?php  
                        require_once('../config.php');
                        $cats = Categoria::getListCat();                           
                        foreach ($cats as $cat)
                        { 
                            echo "<option value=".$cat['id_categoria'].">".$cat['categoria']."</option>";
                        }
                    ?>            
                </select>
            </label>
            <label>
                Titulo
                <input type="text" name="titulo_noticia">
            </label>
            <label>
                imagem
                <input type="file" name="img">
            </label>                
            <label>
                Data
                <input type="date" name="data_noticia">
            </label>
            <label>
                Noticia Ativa
                <input type="checkbox" name="noticia_ativa" checked>
            </label>
            <label>
                Noticia
                <input type="text" name="txt_noticia">
            </label>
            <label>                
                <input type="submit" value="Cadastrar" name="cadastrar_noticia">
            </label>
        </fieldset>
    </form>
</div>
</div>