<?php
    require_once('../config.php');
    //* Inserir administrador
    if(isset($_POST['btn_cadastrar']))
    {
        $adm = new Administrador
        (
            $_POST['txt_nome'],
            $_POST['txt_email'],
            $_POST['txt_login'],
            $_POST['txt_senha']
        );
        $adm->insert();
        if($adm->getId()!=null)
        {
            header('location:principal.php?link=3&msg=ok');
            var_dump($adm);
        }
    }
    //* Deletar Administrador
    $id = filter_input(INPUT_GET,'id');
    $excluir = filter_input(INPUT_GET,'excluir');
    if(isset($id)&& $excluir==1)
    {
        $adm = new Administrador();
        $adm->setId($id);
        $adm->deletar();
        header('location:principal.php?link=3&msg=ok');
        exit;
    }
    //* Alterar administrador
    if(isset($_POST['alterar']))
    {
        $adm = new Administrador();
        $adm->update
        (
            $_POST['id'],
            $_POST['txt_nome'],
            $_POST['txt_email'],
            $_POST['txt_login'] 
        );
        var_dump($_POST);
        header('location:principal.php?link=3&msg=ok');
    }
    //* Efetuar login
    if (isset($_POST['logar']))
    {
        $adm = new Administrador();        
        $adm->efetuarLogin($_POST['txt_login'],$_POST['txt_senha']);        
        if ($adm->getId()>0)
        {
            $_SESSION['logado'] = true;
            $_SESSION['id_adm'] = $adm->getId();
            $_SESSION['nome_adm'] = $adm->getNome();
            $_SESSION['login_adm'] = $adm->getLogin();
            $_SESSION['email_adm'] = $adm->getEmail();
            header('location:principal.php');           
        }
    }
    //* Encerrar a sessão
    if(isset($_GET['sair']))
    {
        if($_GET['sair'])
        {
            $_SESSION['logado'] = false;
            $_SESSION['id_adm'] = null;
            $_SESSION['nome_adm'] = null;
            $_SESSION['login_adm'] = null;
            $_SESSION['email_adm'] = null;
            header('location: index.php');
            exit;
        }            
    }
?>