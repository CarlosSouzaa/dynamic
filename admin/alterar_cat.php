<?php
    $id = filter_input(INPUT_GET,'id');
    $categoria = filter_input(INPUT_GET,'cat');
    $check = filter_input(INPUT_GET,'ativo')
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">    
    <title>Alterar Categoria</title>
    <link rel="stylesheet" href="../admin/css/style.css">
</head>
<body>
    <div id="boxcadastro">
        <div id="formulario-menor"> 
            <form action="op_categoria.php?update=1" method="POST" enctype="multipart/form-data">
            <fieldset>
                <legend>Alterar Categoria</legend>
            <div>
                <input type="hidden" name="id_cat" value="<?php echo $id;?>">
            </div>
            <div>
                <label for="">Categoria</label>
                <input type="text" name="categoria" value="<?php $categoria;?>">
            </div>
            <div>
                <label for="">Ativo</label>
                <input type="checkbox" name="ativo" value="<?php echo $check;?>">
            </div>                
            <div>                
                <input type="submit" name="btn_alterar_categoria" value="Alterar Categoria" class="btn_inserir radius">
            </div>
        </fieldset>
        </form>
        </div>
        </div> 
    </div>   
</body>
</html>