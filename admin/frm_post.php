<div id="box-cadastro">
<div id="formulario-menor">
    <form action="op_post.php" method="post">
        <fieldset>
            <label >
                <span>Categoria</span>
                <select name="id_categoria_post" id="id_categoria_post">            
                    <?php
                        require_once('../config.php'); 
                        $cats = Categoria::getListCat();
                        foreach ($cats as $cat) 
                        {
                            echo "<option value=".$cat['id_categoria'].">".$cat['categoria']."</option>";
                        }
                    ?>            
                </select>
            </label>
            <label>
                <span>Titulo</span>
                <input type="text" name="titulo_post">
            </label>
            <label>
                <span>Descricao</span>
                <input type="text" name="descricao_post">
            </label>
            <label>
                <span>img</span>
                <input type="file" name="img_post">
            </label>                
            <label>
                <span>Data</span>
                <input type="date" name="data_post">
            </label>
            <label>
                <span>Ativo</span>
                <input type="checkbox" name="ativo_post" checked>
            </label>
            <label>
                <input type="submit" value="Cadastrar" name="cadastrar_post">
            </label>
        </fieldset>
    </form>
</div>
</div>